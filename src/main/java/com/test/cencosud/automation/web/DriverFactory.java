package com.test.cencosud.automation.web;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import io.github.bonigarcia.wdm.WebDriverManager;

public abstract class DriverFactory {

	/**
	 * este metodo instancia el driver
	 */
	public static WebDriver getInstance() {
		String driverName = System.getProperty("driver");
		if (driverName == null)
			throw new AssertionError("No driver was specified, use java arg -Ddriver=<driverName>");
		Drivers driverEnum = Drivers.parseFromDriverName(driverName);
		switch (driverEnum) {
		case chrome:
			WebDriverManager.chromedriver().setup();
			return new ChromeDriver();
		case edge:
			WebDriverManager.edgedriver().setup();
			return new EdgeDriver();
		case firefox:
			WebDriverManager.firefoxdriver().setup();
			return new FirefoxDriver();
		case opera:
			WebDriverManager.operadriver().setup();
			return new OperaDriver();
		default:
			String driverValues = Stream.of(Drivers.values()).map(v -> v.name()).collect(Collectors.joining(", "));
			throw new AssertionError(
					"Driver " + driverName + " is not a valid value and one of the following values : " + driverValues);
		}
	}
}
