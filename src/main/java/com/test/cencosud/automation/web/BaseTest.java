package com.test.cencosud.automation.web;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

/**
 * @author alejandro.arevalo
 *
 * Esta clase maneja el driver
 */
public abstract class BaseTest {

	protected WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {

		driver = DriverFactory.getInstance();
		driver.get("https://www.google.cl");
	}

	@AfterTest
	public void afterTest() {
		if (driver != null)
			driver.quit();
	}
}
