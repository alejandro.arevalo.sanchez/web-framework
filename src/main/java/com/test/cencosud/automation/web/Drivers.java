package com.test.cencosud.automation.web;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Drivers {

	chrome, firefox, opera, edge;

	public static Drivers parseFromDriverName(String driverName) {
		Optional<Drivers> optDriver = Stream.of(values()).filter(d -> d.name().equals(driverName)).findFirst();
		if (optDriver.isPresent())
			return optDriver.get();
		throw new AssertionError("Driver " + driverName + " is not a valid value and one of the following values : "
				+ Stream.of(Drivers.values()).map(v -> v.name()).collect(Collectors.joining(", ")));
	}
}
