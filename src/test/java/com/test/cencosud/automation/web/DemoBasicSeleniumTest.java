package com.test.cencosud.automation.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemoBasicSeleniumTest {

	WebDriver driver;

	@BeforeMethod
	public void beforeMethod() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.google.cl");
	}

	@AfterTest
	public void afterTest() {
		if (driver != null)
			driver.quit();
	}

	@Test
	public void demoTest() {
		driver.findElement(By.name("q")).sendKeys("Pachuco");
		System.out.println(driver.getTitle());
	}
}
